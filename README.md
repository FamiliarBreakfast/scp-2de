SCP-2DE is a game set in the SCP Foundation universe. It is also the successor to my older attempt, [SCP-2D](gitlab.com/FamiliarBreakfast/SCP-2D). While SCP-2D was created using pygame in my own custom (but crappy) engine, SCP-2DE is created in Defold.

This repository will contain the Defold project files and build. 
  
More information at my [subreddit](https://www.reddit.com/r/scp2d)
 
Changelog:

Release 0.0.1:
    *Note: Just starting with Defold, learning the basics. Don't expect much for awhile.*
    
    -Background image using tilemaps.
    -That's it.
